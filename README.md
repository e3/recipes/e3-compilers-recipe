# e3-compilers conda recipe

Home: https://gitlab.esss.lu.se/e3-recipes/e3-compilers

Package license: BSD

Recipe license: BSD 3-Clause

Summary: A metapackage to install e3 compilers

This package is a convenience ONLY for users. It can be used to install the compilers and compile locally in a conda environment.
It shouldn't be used when using conda-build. Do NOT use this package as a build or host dependency in a recipe.

This recipe is based on https://github.com/conda-forge/compilers-feedstock.
It only includes the c anc cxx compilers for Linux (no fortran and no OSX/Windows).
